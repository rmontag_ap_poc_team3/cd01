Defines a folder structure for setting up software in VMs organized by environment (develop, test, staging, production)

1) Adapt inventories/develop
2) Call "deploy-docker.sh"
3) Log in to VM with "remote user"
4) sudo su - "action user"
5) Call docker as "action user"

------------------------------------------------

Define different ansible groups/roles:

role: "common"
- setup non-sudo user for starting processes that do noot need too much access rights on system

role: "docker"
- download docker
- install docker
- add "remote user" to docker group
- add "action user" to docker group

---------------------------------------------

Folder structure:

cd01:	// root folder
  
  group_vars:	// storing vriables according to different environments
    develop.yml
    test.yml
    staging.yml
    production.yml

  inventories:	// grouping hosts according to environments
    develop
    test
    staging
    production

  roles:	// defining roles an their related tasks
    common:	// handling common settings like: setup action user
      tasks:
        main.yml
    docker:	// handling docker installation
      tasks:
        main.yml

  deploy-docker.yml	// the main playbook for the task 'deploy-docker'
  deploy-docker.sh	// shell script wrapping the corresponding .yml for environment 'develop'


